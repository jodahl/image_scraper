# DEPRECIATED
This no longer works, as Unsplash has changed their website from when this was written in 2014.

## README

This little script will scrape images from the website [Unsplash](http://www.unsplash.com). It saves them to a folder in the directory the script is run in. It could probably be updated to include functionality to scrape from any Tumblr site.

Currently it saves files in the format PhotographerName-any-tags-on-the-image.jpg. 

#### Dependencies
You need the [Beautiful Soup 4](http://www.crummy.com/software/BeautifulSoup/) Python module. This script is written for Python 2.7.

You can install it with *pip install beautifulsoup4* or *easy_install beautifulsoup4*.

#### Usage

Options:
update - Gets latest pictures, will get all images on first run
all - Gets all images again, currently not working
Example:
python scrape.py update will get the latest pictures since the last update

Run the script without an argument to display the usage

#### License
Use as you will, no responsibility will be taken for how it works on my part.

