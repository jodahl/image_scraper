#!/bin/python/
__author__ = "Josiah Dahl"

"""
    This little script scrapes images from the website www.unsplash.com,
    which provides royalty free hi-resolution images.

    File name system:
        Name of photographer followed by any tags in place
"""

import urllib2
import os
import sys
import pickle
import string
from bs4 import BeautifulSoup

class Scraper:
    def __init__(self, argument, dl_dir="downloaded"):
        self.num_downloaded = 0
        self.page_number = 1
        self.dl_log = set() # Set of all downloaded images
        self.argument = argument
        self.dl_dir = dl_dir
        self.log_file = self.dl_dir + '/images.log'
        # Get current directory
        self.cur_dir = os.getcwd()
        self.dl_dir = os.path.join(self.cur_dir, self.dl_dir)
        # Check for first run/dl directory existence
        if not os.path.isdir(self.dl_dir):
            print "First run or starting in a different location."
            print "Creating directory {0}".format(self.dl_dir)
            try:
                os.makedirs(self.dl_dir)
            except os.error:
                sys.exit("Could not create directory {0}.\nExiting".format(self.dl_dir))
        # Read the log file
        try:
            with open(self.log_file,'r') as f:
                for url in f:
                     self.dl_log.add(url.strip('\n'))
                print "Log file loaded."
        except IOError:
            # Called when file does not exist, can just ignore
            print "Creating new log file."
            with open(self.log_file, 'w') as f:
                pass


    def download(self):
        while True:
            # Get image url and potential filename
            post_info = self.parse_page('http://www.unsplash.com/page/' + str(self.page_number))
            if not post_info:
                print "Reached the end of the website!"
                break
            # Download and save the images. Return a command to keep going based on
            # How many passed URLS have already been downloaded
            if self.download_image(post_info):
                self.page_number = self.page_number + 1
            else:
                break
        print "{0} images downloaded successfully.".format(self.num_downloaded)


    def parse_page(self,url):
        """ Returns a tuple of (image url, filename to save as) """
        # Parse the page using BeautifulSoup, get image links
        # Posts held in <ul id="posts">, traverse through children
        page_html = urllib2.urlopen(url).read()
        soup = BeautifulSoup(page_html)
        # Find all the posts
        raw_posts = soup.find_all('li', {'class':'post'} )
        posts = []
        # Go through posts to find all relevant information
        for post in raw_posts:
            # Get image url
            src = post.find('img', {'class':'photo_img'}).get('src')
            tags = []
            raw_tags = post.find_all('li',{'class':'tag'})
            # Get image tags
            for tag in raw_tags:
                if tag is not None:
                    tags.append(str(tag.find('a').get_text().strip('#')))
            # Get photographer - This shit is so convoluted but it works!
            name = post.find('img', {'class':'photo_img'}).get('alt')[14:] \
                .replace(u"\xa0", " ").encode('utf-8').strip('y ').split(' ')
            # Make the filename
            filename = ''
            for n in name:
                filename = filename + str(n)
            filename = filename + '-'
            for tag in tags:
                filename = filename + str(tag)
                filename = filename + '-'
            posts.append((src, filename))

        return posts


    def download_image(self,post_info):
        # number of already downloaded URLs, stop downloading if it's 9
        gotten_already = 0
        for post in post_info:
            src, filename = post
            # Check if filename is not yet created
            letters = string.ascii_lowercase
            let_count = 0
            while os.path.isfile(self.dl_dir+'/'+filename+'.jpg'):
                filename = filename[:-1] + letters[let_count]
                let_count = let_count + 1
            try:
                # Check if the url has arlready been downloaded
                if src in self.dl_log:
                    gotten_already = gotten_already + 1
                else:
                    with open(self.dl_dir+'/'+filename+'.jpg','wb') as f:
                        f.write(urllib2.urlopen(src).read())
                    print filename + '.jpg downloaded successfully'
                    self.num_downloaded = self.num_downloaded + 1
                    # Add the downloaded URL to the log
                    with open(self.log_file, 'a') as f:
                        f.write(src+'\n')
                        self.dl_log.add(src)
            except IOError:
                print 'SOmething went wrong'

        if gotten_already < 9:
            return True
        else:
            return False

def show_usage():
    print " ----------------------------------------------------------------- "
    print "| Currently this script is optimized for www.unsplash.com.        |"
    print "| Downloaded images are stored in ./downloaded/                   |"
    print "| A list of downloaded files is stored in ./downloaded/images.log |"
    print " ----------------------------------------------------------------- "
    print "\nOptions:"
    print "\tupdate\t - Gets latest pictures, will get all images on first run"
    print "\tall\t - Gets all images again, currently not working"
    print "\nExample:"
    print "\t'python scrape.py update' will get the latest pictures since the last update"
    print ""


def main():
    sys_args = {'update','all'}
    if len(sys.argv) <= 1:        
        show_usage()
    elif sys.argv[1] in sys_args:
        s = Scraper(sys.argv[1])
        s.download()

if __name__ == "__main__":
    main()


